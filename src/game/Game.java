package game;

import java.util.Random;
import org.fusesource.jansi.AnsiConsole;
import org.fusesource.jansi.Ansi;
import org.fusesource.jansi.Ansi.Color;

import player.Player;
import player.maurizio.MaurizioAI;
import player.malte.MalteAI;

/**
 * Represents a game of Connect Four.
 */
public class Game {

    /**
     * Number of columns on the board.
     */
    public final static int GAME_COLUMNS = 7;

    /**
     * Number of rows on the board.
     */
    public final static int GAME_ROWS = 6;

    /**
     * Player One
     */
    private final PlayerObject p1;

    /**
     * Player Two
     */
    private final PlayerObject p2;

    /**
     * The current player.
     */
    private PlayerObject currentP;

    /**
     * The game's history.
     */
    private final GameHistory hist;

    /**
     * The game's board.
     * It's size is GAME_COLUMNS * GAME_ROWS.
     * 0 - Empty Space
     * 1 - Player One's piece
     * 2 - Player Two's piece
     */
    private int[][] board;

    /**
     * Is the game still running?
     */
    private boolean gameOn;

    /**
     * Constructor.
     * Initialize board and save players.
     *
     * @param p1 Player One
     * @param p2 Player Two
     */
    public Game(Player p1, Player p2) {
        this.p1 = new PlayerObject(1, p1);
        this.p2 = new PlayerObject(2, p2);
        this.hist = new GameHistory();
        this.board = new int [GAME_COLUMNS][GAME_ROWS];
        this.gameOn = false;
        // Set enemy IDs
        this.p1.setEnemyID(2);
        this.p2.setEnemyID(1);
    }

    /**
     * Simulates n games.
     * Simulates the games and prints statistical output to stdout.
     *
     * @param runs The number of games to simulate.
     */
    public static void simulate(int runs) {
        Player p1 = new MaurizioAI("Maurizio");
        Player p2 = new MalteAI("Malte");
        // 0 - Draw
        // 1 - p1 wins
        // 2 - p2 wins
        int[] statistic = new int[3];
        for (int i = 0; i < runs; i++) {
            int result = new Game(p1, p2).start(false, true);
            // TODO: Improve IDs
            statistic[result]++;
        }
        double[] percents = new double[3];
        for (int i = 0; i < statistic.length; i++) {
            percents[i] = ((double)statistic[i]) / runs * 100.0;
        }
        System.out.println("Draws   : " + statistic[0]);
        System.out.println(p1.getName() + ": " + statistic[1] + " wins");
        System.out.println(p2.getName() + ": " + statistic[2] + " wins");

        System.out.println("PERCENTAGE:");
        System.out.println("Draws   : " + percents[0] + "%");
        System.out.println(p1.getName() + ": " + percents[1] + "% wins");
        System.out.println(p2.getName() + ": " + percents[2] + "% wins");

    }

    /**
     * Start a game.
     * @param output determines, whether game info etc. gets printed to the console
     * @param rand determines whether the starting player gets selected randomly
     * @return the winner of the game (0=draw)
     */
    public int start(boolean output, boolean rand) {
        // Initialize Jansi
        AnsiConsole.systemInstall();
        // Set starting player.
        if(rand){
            Random ran = new Random();
            if (ran.nextBoolean()) {
                currentP = this.p1;
            } else {
                currentP = this.p2;
            }
        } else{
            currentP = this.p1;
        }

        // Start the game
        this.gameOn = true;
        while(gameOn) {
            if(makeMove(output)){
                currentP = other(currentP);
                if(output){
                    logGame(hist.getLast());
                }
                int state = checkState(output);
                if(state != (-1)){
                    if(output){
                        System.out.println("*******************");
                        System.out.println("Thanks for Playing!");
                        System.out.println("*******************");
                    }
                    // Uninstall Jansi
                    AnsiConsole.systemUninstall();
                    return state; // the result of the game
                }
	    } else{
	        // an illegal move was made
		if(output){
		    System.out.println(other(currentP).getP().getName() + " wins.");
		}
		return other(currentP).getID(); // TODO only works if the players have IDs 1 and 2
	    }
        }
        // Uninstall Jansi
        AnsiConsole.systemUninstall();
        System.out.println("BUG!");
        return checkState(false); // can't be reached
    }

    /**
     * Returns the opponent of the given player.
     *
     * @param p The player.
     * @return The opponent of the given player.
     */
    private PlayerObject other(PlayerObject p) {
        if (p1.equals(p)) {
            return p2;
        } else {
            return p1;
        }
    }

    /**
     * Calls a players functions to make a move.
     */
    private boolean makeMove(boolean output) {
        // Get a choice from the player, while only giving him a copy of the game.
        int choice = currentP.getP().move(copyBoard());
        // Check his choice against the current board.
        if (choice < 0 || choice > GAME_COLUMNS || this.board[choice][0] != 0) {
            // If a player makes a false move, the game punishes him.
            this.gameOn = false;
            if(output){
                log(currentP.getP().getName() + " made an illegal move and lost!");
            }
            return false;
        }
        // Find the lowest empty field in the board in the choosen column.
        int pos = 0;
        while (pos < (board[0].length - 1) && this.board[choice][pos + 1] == 0) {
            pos++;
        }
        // Change the board accordingly.
        this.board[choice][pos] = currentP.getID();
        this.hist.add(new GameEntry(currentP, choice, pos));
        if(output){
            log(currentP.getP().getName() + " made a move!");
        }
	return true;
    }

    /**
    * Resets the board and the history of the game.
    */
    public void reset(){
        this.board = new int[GAME_COLUMNS][GAME_ROWS];
    }
    /**
     * Checks the board for game-ending conditions.
     * The game is won, if one of the players manages to get four
     * of his pieces in a row.
     * The game is drawn when the board is full, that is, when every
     * one of the 42 pieces are played.
     * The function changes variables accordingly.
     * @return the state of the game, -1 = still undecided , 0 = draw, 1 = Player 1 wins, ...
     */
    private int checkState(boolean output) {
        if(checkWin(this.board, p1.getID())){
            this.gameOn = false;
            if(output){
                System.out.println(p1.getP().getName() + " wins!");
            }
            return p1.getID(); // player 1 wins
        }
        if(checkWin(this.board, p2.getID())){
            this.gameOn = false;
            if(output){
                System.out.println(p2.getP().getName() + " wins!");
            }
            return p2.getID(); // player 2 wins
        }

        boolean draw = true;
        for (int i = 0; i < GAME_COLUMNS; i++) {
            if(this.board[i][0] == 0) {
                draw = false;
                break;
            }
        }
        if (draw) {
            this.gameOn = false;
            if(output){
                System.out.println("Draw!");
            }
            return 0; // draw
        }
        return -1; // game still undecided
    }

    /**
     * Creates a deep copy of the board.
     *
     * @return A deep copy of the board.
     */
    private int[][] copyBoard() {
        int[][] ret = new int[GAME_COLUMNS][GAME_ROWS];
        for (int i = 0; i < GAME_COLUMNS; i++) {
            for (int j = 0; j < GAME_ROWS; j++) {
                ret[i][j] = this.board[i][j];
            }
        }
        return ret;
    }

    /**
     * Log player actions.
     */
    private void log(String s) {
        System.out.println(s);
    }

    /**
     * Prints the current board to stdout.
     */
    private void logGame(GameEntry entry) {
        Ansi playerX = Ansi.ansi().render("|@|red X|@");
        Ansi playerO = Ansi.ansi().render("|@|green O|@");
        Ansi playerXBold = Ansi.ansi().render("|@|magenta,bold X|@");
        Ansi playerOBold = Ansi.ansi().render("|@|magenta,bold O|@");
        for(int i = 0; i < board[0].length; i++){
            for(int j = 0;j < board.length; j++){
                if(i == entry.getRow() && j == entry.getColumn()) {
                    if(board[j][i] == 1){
                        System.out.print(playerXBold);
                    }
                    if (board[j][i] == 2){
                        System.out.print(playerOBold);
                    }
                } else {
                    if(board[j][i] == 1){
                        System.out.print(playerX);
                    }
                    if (board[j][i] == 2){
                        System.out.print(playerO);
                    }
                }
                if(board[j][i] == 0){
                    System.out.print("| ");
                }
            }
            System.out.println("|");
        }
        System.out.println("+-+-+-+-+-+-+-+");
        for (int i = 0; i < board.length; i++) {
            System.out.print("|" + i);
        }
        System.out.println("|");
    }

    public static boolean checkWin(int[][] board, int player){
        int winLength = 4;
        boolean win = false;
        // check columns for win
        for(int i=0;i<board.length;i++){
            for(int j=0;j<(board[0].length-winLength+1);j++){
                if(board[i][j] == player){
                    win = true;
                    for(int k=1;k<winLength;k++){
                        if(board[i][j+k] != board[i][j]){
                            win = false;
                            break;
                        }
                    }
                    if(win){
                        return true;
                    }
                }
            }
        }

        // check rows for win
        for(int i=0;i<board[0].length;i++){
            for(int j=0;j<(board.length-winLength+1);j++){
                if(board[j][i] == player){
                    win = true;
                    for(int k=1;k<winLength;k++){
                        if(board[j+k][i] != board[j][i]){
                            win = false;
                            break;
                        }
                    }
                    if(win){
                        return true;
                    }
                }
            }
        }

        // check diagonals for win
        //bottom-left to top-right diagonals
        for(int i=0;i<(board.length-winLength+1);i++){
            for(int j=(winLength-1);j<board[0].length;j++){
                if(board[i][j] == player){
                    win = true;
                    for(int k=1;k<winLength;k++){
                        if(board[i+k][j-k] != board[i][j]){
                            win = false;
                            break;
                        }
                    }
                    if(win){
                        return true;
                    }
                }
            }
        }

        //top-left to bottom-right diagonals
        for(int i=0;i<(board.length-winLength+1);i++){
            for(int j=0;j<(board[0].length-winLength+1);j++){
                if(board[i][j] == player){
                    win = true;
                    for(int k=1;k<winLength;k++){
                        if(board[i+k][j+k] != board[i][j]){
                            win = false;
                            break;
                        }
                    }
                    if(win){
                        return true;
                    }
                }
            }
        }
        return false;
    }

}
