package game;

public class GameEntry {

    private final PlayerObject p;
    private final int column;
    private final int row;

    public GameEntry(PlayerObject p, int column, int row) {
        this.p = p;
        this.column = column;
        this.row = row;
    }

    public PlayerObject getPlayer() {
        return this.p;
    }

    public int getColumn() {
        return this.column;
    }

    public int getRow() {
        return this.row;
    }
}
