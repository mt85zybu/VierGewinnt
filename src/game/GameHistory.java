package game;

import java.util.List;
import java.util.ArrayList;

public class GameHistory {

    private List<GameEntry> hist;

    public GameHistory() {
        hist = new ArrayList<>();
    }

    public void add(GameEntry ge) {
        this.hist.add(ge);
    }

    public GameEntry getLast() {
        if (this.hist.size() > 0) {
            return this.hist.get(this.hist.size() - 1);
        }
        return null;
    }

    public PlayerObject lastPlayer() {
        if (this.hist.size() > 0) {
            return getLast().getPlayer();
        }
        return null;
    }
}
