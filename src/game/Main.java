package game;

import player.Player;
import player.Human;
import player.maurizio.MaurizioAI;
import player.malte.MalteAI;

public class Main {

    public static void main(String[] args) {
        Player p1, p2;
        if (args.length == 2) {
            switch (args[0].toLowerCase()) {
            case "maurizio":
                p1 = new MaurizioAI("Maurizio");
                break;
            case "human":
                p1 = new Human();
                break;
            case "malte":
            default:
                p1 = new MalteAI("Malte");
            }
            switch (args[1].toLowerCase()) {
            case "malte":
                p2 = new MalteAI("Malte");
                break;
            case "human":
                p2 = new Human();
                break;
            case "maurizio":
            default:
                p2 = new MaurizioAI("Maurizio");
            }
            new Game(p1, p2).start(true, true); //1. param = output mode, 2. param = random beginner
        } else if (args.length == 1) {
            Game.simulate(Integer.parseInt(args[0]));
        } else {
            p1 = new MalteAI("Malte");
            p2 = new MaurizioAI("Maurizio");
            new Game(p1, p2).start(true, true); //1. param = output mode, 2. param = random beginner
        }
    }
}
