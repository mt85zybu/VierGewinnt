package game;

import player.Player;

/**
 * Wrapper for the player.
 * This is a wrapper for a player. This guarantees, that no
 * player will change his Symbol, nor his id.
 */
public class PlayerObject {

    private final Player p;
    private final int id;
    private int enemyID;
    private String sym;

    /**
     * Wraps a player.
     *
     * @param id The players id.
     * @param p The player himself.
     */
    public PlayerObject(int id, Player p) {
        this.p = p;
        this.id = id;
        this.p.setPlayerID(id);
    }

    public void setSymbol(String sym) {
        this.sym = sym;
    }

    public String getSymbol() {
        return this.sym;
    }

    public Player getP() {
        return this.p;
    }

    public void setEnemyID(int id) {
        this.enemyID = id;
        this.p.setEnemyID(id);
    }

    public int getID() {
        return this.id;
    }
}
