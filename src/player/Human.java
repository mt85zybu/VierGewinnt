package player;

import java.util.Scanner;
import java.util.HashSet;
import java.util.Set;
import java.util.Arrays;

public class Human implements Player {

    private Scanner sc;
    private int id;
    private int enemyID;
    private String name;

    public Human() {
        sc = new Scanner(System.in);
        System.out.print("Bitte gib deinen Namen ein: ");
        this.name = sc.nextLine();
    }

    @Override
    public int move(int[][] board) {
        Set<Integer> options = new HashSet<>(Arrays.asList(0,1,2,3,4,5,6));
        for (Integer i: new HashSet<Integer>(options)) {
            if (board[i][0] != 0) {
                options.remove(i);
            }
        }
        int choice = -1;
        // make sure that the user chooses a legal move
        while (!options.contains(new Integer(choice))) {
            System.out.print("Choose a move from " + options + ": ");
            System.out.flush();
            choice = sc.nextInt();
            if(!options.contains(new Integer(choice))){
                System.out.println("This is not a legal move!");
            }
        }
        return choice;
    }

    @Override
    public void setPlayerID(int id) {
        this.id = id;
    }

    @Override
    public void setEnemyID(int enemyID) {
        this.enemyID = enemyID;
    }

    @Override
    public String getName() {
        return this.name;
    }
}
