package player;

public interface Player {

    public int move(int[][] game);
    public String getName();
    public void setPlayerID(int id);
    public void setEnemyID(int id);
}
