package player.malte;

public class Item {

    public final Position pos;
    public final int id;
    public final boolean hasScaffold;
    public final boolean onBoard;

    public Item(Position pos,
                int id,
                boolean hasScaffold,
                boolean onBoard) {
        this.pos = pos;
        this.id = id;
        this.hasScaffold = hasScaffold;
        this.onBoard = onBoard;
    }

    public String idString(int playerID, int enemyID) {
        String s = "";
        if (!onBoard) {
            s = "!";
        } else if (this.id == playerID) {
            s = "M";
        } else if (this.id == enemyID) {
            s = "E";
        } else if (hasScaffold) {
            s = "O";
        } else {
            s = " ";
        }
        return s;
    }
}
