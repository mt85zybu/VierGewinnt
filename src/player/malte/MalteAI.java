package player.malte;

import java.util.Random;
import java.util.Set;
import java.util.HashSet;
import java.util.Arrays;
import java.lang.Math;

import player.Player;
import game.Game;

/**
 * Maltes artificial intelligence for playing a game of Connect Four
 */
public class MalteAI implements Player{

    /**
     * Name of the player.
     */
    private String name;

    /**
     * Random Object.
     */
    private Random ran;

    /**
     * The players ID.
     */
    private int id;

    /**
     * The enemy's ID.
     */
    private int enemyID;

    /**
     * Constructor.
     *
     * @param name The name for the player.
     */
    public MalteAI(String name){
        this.name = name;
        this.ran = new Random();
    }

    @Override
    public void setPlayerID(int id) {
        this.id = id;
        this.enemyID = id == 1 ? 2: 1;
    }

    @Override
    public void setEnemyID(int enemyID) {
        this.enemyID = enemyID;
    }

    @Override
    public int move(int[][] board){
        double[] weights = new double[Game.GAME_COLUMNS];
        for (int i = 0; i < weights.length; i++) {
            weights[i] = calculateWeight(board, i);
        }
        double max = -1;
        for (double d: weights) {
            max = max > d ? max: d;
        }
        System.out.println(Arrays.toString(weights));
        for (int i = 0; i < weights.length; i++) {
            if (Math.abs(max - weights[i]) < 0.000001) {
                return i;
            }
        }
            return 0;
    }

    public double calculateWeight(int[][] board, int column) {
        double W_ILLEGAL            =  Double.MIN_VALUE;
        double W_DONT               =  0.01;
        double weight               =  1.0;
        // Prever a place in the center;
        weight *= 1.0 - Math.abs(3.0 - column) / (3.0 * 100.0);
        // Relative positions like (-1, -1), (0, -1), etc.
        Position[] relAround = Position.getRelCirclePositions();
        // The position we're looking at
        Position thisPos = getLastEmpty(board, column);
        // There is no space left in this row.
        if (thisPos == null) {
            weight *= W_ILLEGAL;
            return weight;
        }
        // Setup Sequences
        Sequence[] sequences = new Sequence[8];
        Sequence[] sequencesAbove = new Sequence[8];
        int[][] futureBoard = makeMove(copyBoard(board), column, this.id);
        for (int i = 0; i < sequences.length; i++) {
            sequences[i] = Sequence.readSequenceFromBoard(board,
                                                          thisPos.add(relAround[i]),
                                                          relAround[i],
                                                          this.id,
                                                          this.enemyID);
            sequences[i].setPlayerID(this.id);
            sequences[i].setEnemyID(this.enemyID);
            sequencesAbove[i] = Sequence.readSequenceFromBoard(futureBoard,
                                                               thisPos.add(relAround[0]).add(relAround[i]),
                                                               relAround[i],
                                                               this.id,
                                                               this.enemyID);
            sequencesAbove[i].setPlayerID(this.id);
            sequencesAbove[i].setEnemyID(this.enemyID);
        }
        for (int i = 0; i < sequences.length / 2; i++) {
            Sequence x = sequences[i];
            Sequence y = sequences[i + 4];
            weight *= getBaseWeightForSequences(x, y);
            Sequence xA = sequencesAbove[i];
            Sequence yA = sequencesAbove[i + 4];
            // weight *= getBaseWeightForSequences(xA, yA);
            if ((xA.matches("EEE") ||
                 yA.matches("EEE")) ||
                ((xA.matches("E") &&
                  yA.matches("EE")) ||
                 (xA.matches("EE") &&
                  yA.matches("E")))) {
                // TODO: Do this right!
                weight /= 2<<6;
            }
        }
        return weight;
    }

    private double getBaseWeightForSequences(Sequence x, Sequence y) {
        double W_PRIORITY_1         =  2 << 8;
        double W_PRIORITY_2         =  2 << 6;
        double W_PRIORITY_3         =  2 << 4;
        double W_PRIORITY_4         =  2 << 2;
        double W_PRIORITY_5         =  2 << 0;
        double W_PRIORITY_6         =  1.0;
        double W_START              =  1.0;
        double weight = W_START;
        if (x.matches("MMM") ||
            y.matches("MMM")) {
            weight *= W_PRIORITY_1;
        } else if ((x.matches("M") &&
                    y.matches("MM")) ||
                   (x.matches("MM") &&
                    y.matches("M"))) {
            weight *= W_PRIORITY_1;
        } else if (x.matches("EEE") ||
                   y.matches("EEE")) {
            weight *= W_PRIORITY_2;
        } else if ((x.matches("E") &&
                    y.matches("EE")) ||
                   (x.matches("EE") &&
                    y.matches("E"))) {
            weight *= W_PRIORITY_2;
        } else if ((x.matches("O") &&
                    y.matches("MMO")) ||
                   (x.matches("MMO") &&
                    y.matches("O"))) {
            weight *= W_PRIORITY_3;
        } else if ((x.matches("O") &&
                    y.matches("EEO")) ||
                   (x.matches("EEO") &&
                    y.matches("O"))) {
            weight *= W_PRIORITY_4;
        } else if (x.matches("EO") &&
                   y.matches("EO")) {
            weight *= W_PRIORITY_4;
        } else if (x.matches("MMO") ||
                   y.matches("MMO")) {
            weight *= W_PRIORITY_5;
        } else if ((x.matches("O") &&
                    y.matches("MM")) ||
                   (x.matches("MM") &&
                    y.matches("O"))) {
            weight *= W_PRIORITY_5;
        } else if ((x.matches("M") &&
                    y.matches("MO")) ||
                   (x.matches("MO") &&
                    y.matches("M"))) {
            weight *= W_PRIORITY_5;
        } else if (x.matches("EEO") ||
                   y.matches("EEO")) {
            weight *= W_PRIORITY_6;
        } else if ((x.matches("O") &&
                    y.matches("EE")) ||
                   (x.matches("EE") &&
                    y.matches("O"))) {
            weight *= W_PRIORITY_6;
        } else if ((x.matches("E") &&
                    y.matches("EO")) ||
                   (x.matches("EO") &&
                    y.matches("E"))) {
            weight *= W_PRIORITY_6;
        }
        return weight;
    }

    /**
     *
     */
    private Position getLastEmpty(int[][] board, int column) {
        if (board[column][0] != 0) {
            return null;
        }
        int row = 0;
        while (row < Game.GAME_ROWS - 1 &&
               board[column][row + 1] == 0) {
            row++;
        }
        return new Position(column, row);
    }

    /**
     *
     */
    private boolean isPosOnBoard(Position pos) {
        if (pos.getPosX() <= Game.GAME_COLUMNS - 1 &&
            pos.getPosX() >= 0 &&
            pos.getPosY() <= Game.GAME_ROWS - 1 &&
            pos.getPosY() >= 0) {
            return true;
        }
        return false;
    }

    /**
     *
     */
    private int getIDFromBoard(int[][] board, Position pos) {
        return board[pos.getPosX()][pos.getPosY()];
    }

    /**
     * Copies the given board.
     *
     * @param board The board to be copied.
     * @return A deep copy of the board.
     */
    private int[][] copyBoard(int[][] board) {
        int[][] copy = new int[board.length][board[0].length];
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                copy[i][j] = board[i][j];
            }
        }
        return copy;
    }

    /**
     * Makes a move on the given board.
     * Makes a move on the given board, no winning check or anything,
     * just a fake move on the given board. Necessary for confirming that
     * a chosen move will not give the enemy an instant win.
     *
     * @param board The board on which the move is made.
     * @param choice The column to play in.
     * @param id The player's id.
     * @return The modified board.
     */
    private int[][] makeMove(int[][] board, int choice, int id) {
        int row = 0;
        // If the column is full, do nothing.
        if (board[choice][row] != 0) {
            return board;
        }
        // Find the last empty row.
        while (row < Game.GAME_ROWS - 1 && board[choice][row + 1] == 0) {
            row++;
        }
        // Add the players piece.
        board[choice][row] = id;
        return board;
    }

    /**
     * Get the player's name.
     */
    public String getName(){
        return this.name;
    }
}
