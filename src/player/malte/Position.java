package player.malte;

import java.lang.String;

public class Position {

    private final int posX;
    private final int posY;

    public Position(int posX, int posY) {
        this.posX = posX;
        this.posY = posY;
    }

    public int getPosX() {
        return this.posX;
    }

    public int getPosY() {
        return this.posY;
    }

    public Position add(Position pos) {
        return new Position(this.getPosX() + pos.getPosX(),
                            this.getPosY() + pos.getPosY());
    }

    @Override
    public String toString() {
        return String.format("(%+d, %+d)", posX, posY);
    }

    public static Position[] getRelCirclePositions() {
        return new Position[]{
            new Position(0, -1),
            new Position(1, -1),
            new Position(1, 0),
            new Position(1, 1),
            new Position(0, 1),
            new Position(-1, 1),
            new Position(-1, 0),
            new Position(-1, -1)
        };
    }
}
