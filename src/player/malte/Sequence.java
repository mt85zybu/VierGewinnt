package player.malte;

import java.util.List;
import java.util.ArrayList;

import game.Game;

public class Sequence {

    private Position startingPos;
    private Position direction;
    private List<Item> items;
    private int playerID;
    private int enemyID;

    public Sequence(Position startingPos, Position direction, List<Item> items) {
        this.items = new ArrayList<Item>(items);
        this.startingPos = startingPos;
        this.direction = direction;
    }

    public void setPlayerID(int playerID) {
        this.playerID = playerID;
    }

    public void setEnemyID(int enemyID) {
        this.enemyID = enemyID;
    }

    public boolean matches(String s) {
        String idStr = idString().toLowerCase();
        s = s.toLowerCase();
        for (int i = 0; i < s.length(); i++) {
            if (i >= idStr.length()) {
                return false;
            }
            if (s.charAt(i) == 'p' &&
                (idStr.charAt(i) != 'm' &&
                 idStr.charAt(i) != 'e')) {
                return false;
            } else if (s.charAt(i) != idStr.charAt(i)) {
                return false;
            }
        }
        return true;
    }

    public static Sequence readSequenceFromBoard(int[][] board,
                                                 Position pos,
                                                 Position dir,
                                                 int playerID,
                                                 int enemyID) {
        Position cur = pos;
        List<Item> items = new ArrayList<Item>();
        Position down = new Position(0, 1);
        while (isPosOnBoard(cur)) {
            boolean hasScaffold = (isPosOnBoard(cur.add(down)) &&
                                   getIDFromBoard(board, cur.add(down)) != 0) || cur.getPosY() == 5;
            items.add(new Item(cur,
                               getIDFromBoard(board, cur),
                               hasScaffold,
                               true));
            cur = cur.add(dir);
        }
        items.add(new Item(cur,
                           -1,
                           false,
                           false));
        return new Sequence(pos, dir, items);
    }

    private static boolean isPosOnBoard(Position pos) {
        if (pos.getPosX() <= Game.GAME_COLUMNS - 1 &&
            pos.getPosX() >= 0 &&
            pos.getPosY() <= Game.GAME_ROWS - 1 &&
            pos.getPosY() >= 0) {
            return true;
        }
        return false;
    }

    private static Integer getIDFromBoard(int[][] board, Position pos) {
        return new Integer(board[pos.getPosX()][pos.getPosY()]);
    }

    public String idString() {
        String s = "";
        for (Item x: items) {
            s += x.idString(playerID, enemyID);
        }
        return s;
    }

    @Override
    public String toString() {
        return String.format("START: %s, DIR: %s, \"%s\"", startingPos, direction, idString());
    }
}
