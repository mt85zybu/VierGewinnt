package player.maurizio;

import java.util.Random;
import game.Game;
import java.util.List;
import java.util.LinkedList;

import player.Player;

public class MaurizioAI implements Player {

    private String name;
    private Random ran;
    private int id;
    private int enemyID;

    public MaurizioAI(String name){
        this.name = name;
        this.ran = new Random();
    }

    public void setPlayerID(int id) {
        this.id = id;
    }

    public void setEnemyID(int id) {
        this.enemyID = id;
    }

    public int move(int[][] board){
        // check for one move wins
        int m1 = winningMove(board, id);
	    if(m1 != -1){
	        return m1;
	    }

        // create a list of all legal moves
        LinkedList<Integer> moves1 = new LinkedList<Integer>();
        for(int i=0;i<Game.GAME_COLUMNS;i++){
            if(board[i][0] == 0){
                moves1.add(i);
            }
        }

        // filter out loosing moves
        for(int i=0;i<Game.GAME_COLUMNS;i++){
            if(board[i][0] == 0){
                int[][] boardNew = makeMove(board, i, id);
	    	    int m2 = winningMove(boardNew, enemyID);
	 	        if(m2 != -1){
		            moves1.remove((Object) i);
		        }
            }
        }

        //move randomly if instant defeat is inevitable
        if(moves1.isEmpty()){
            return moveRandom(board);
        }

        //forced move
        if(moves1.size() == 1){
            return moves1.peek();
        }

        //look for two move wins among the non-loosing moves
        int m2 = winningMove2(board, id, moves1);
        if(m2 != -1){
            return m2;
        }

        //copy moves
        LinkedList<Integer> moves2 = new LinkedList<Integer>();
        for(int i : moves1){
            moves2.add(i);
        }
        // avoid 2-move losses
        for(int i : moves1){ // I make a move
            int[][] board1 = makeMove(board, i, id);
            int m3 = winningMove2(board1, enemyID, allMoves());
            if(m3 != -1){
                moves2.remove((Object) i);
            }
        }

        if(!moves2.isEmpty()){
            return moveCentral(board, moves2);
        } else {
            if(!moves1.isEmpty()){
                return moveCentral(board, moves1);
            }
        }

        return moveCentral(board); // can't be reached
    }

    private int winningMove(int [][] board, int id){
         for(int i=0;i<Game.GAME_COLUMNS;i++){
            if(board[i][0] == 0 && checkWin(board, i, id)){
                return i;
            }
         }
	 return -1;// signals that no winning move exists
    }

    private LinkedList<Integer> allMoves(){
        LinkedList<Integer> ret = new LinkedList<Integer>();
        for(int i=0;i<Game.GAME_COLUMNS;i++){
            ret.add(i);
        }
        return ret;
    }

    private int winningMove2(int[][] board, int id, LinkedList<Integer> moves){
        for(int i : moves){
            if(board[i][0] != 0){
                continue;
            }
            int[][] boardNew = makeMove(board, i, id);
            boolean winner1 = true;
            for(int j=0;j<Game.GAME_COLUMNS;j++){
                if(boardNew[j][0] == 0){
                    int[][] boardNewNew = makeMove(boardNew, j, enemyID);
		            int m3 = winningMove(boardNewNew, id);
		            if(m3 == -1){
		                winner1 = false;
			            break;
		            }
                }
            }
            if(winner1){
                return i;
            }
        }
        return -1; // no move found that wins in 2 moves
    }

    private int moveRandom(int[][] board){
        int choice = ran.nextInt(7);
        while (board[choice][0] != 0) {
            choice = ran.nextInt(7);
        }
        return choice;
    }

    private int moveCentral(int[][] board){
        if(board[3][0] == 0){
            return 3;
        }
        if(board[2][0] == 0){
            return 2;
        }
        if(board[4][0] == 0){
            return 4;
        }
        if(board[1][0] == 0){
            return 1;
        }
        if(board[5][0] == 0){
            return 5;
        }
        if(board[0][0] == 0){
            return 0;
        }
        if(board[6][0] == 0){
            return 6;
        }
        return -1; // board is full
    }

    private int moveCentral(int[][] board, LinkedList<Integer> moves){
        if (moves.isEmpty()) {
            return -1; // empty list
        }
        if(moves.contains(3) && board[3][0] == 0){
            return 3;
        }
        if(moves.contains(2) && board[2][0] == 0){
            return 2;
        }
        if(moves.contains(4) && board[4][0] == 0){
            return 4;
        }
        if(moves.contains(1) && board[1][0] == 0){
            return 1;
        }
        if(moves.contains(5) && board[5][0] == 0){
            return 5;
        }
        if(moves.contains(0) && board[0][0] == 0){
            return 0;
        }
        if(moves.contains(6) && board[6][0] == 0){
            return 6;
        }

        return -1; // Can' t happen
    }

    private int[][] makeMove(int[][] board, int choice, int id) {
        // Check his choice against the current board.
        if (choice < 0 || choice > Game.GAME_COLUMNS || board[choice][0] != 0) {
            System.err.println("Illegal Move!");
            return null;
        }
        // Find the lowest empty field in the board in the choosen column.
        int pos = 0;
        while (pos < (board[0].length - 1) && board[choice][pos + 1] == 0) {
            pos++;
        }
        // Change the board accordingly
        int[][] boardNew = copyBoard(board);
        boardNew[choice][pos] = id;
        return boardNew;
    }

    private int[][] copyBoard(int[][] board) {
        int[][] ret = new int[Game.GAME_COLUMNS][Game.GAME_ROWS];
        for (int i = 0; i < Game.GAME_COLUMNS; i++) {
            for (int j = 0; j < Game.GAME_ROWS; j++) {
                ret[i][j] = board[i][j];
            }
        }
        return ret;
    }

    private boolean withinBoard(int x, int y){
        return ((x>=0 && x < Game.GAME_COLUMNS) && (y>=0 && y < Game.GAME_ROWS));
    }

    /**
    * checks whether the last move closed any lines on the board
    */
    private boolean checkWin(int[][] board, int col, int iD){
        // find row of last move -- assuming the move was legal
        int row = 0;
        while(row < Game.GAME_ROWS-1 && board[col][row+1] == 0){
            row++;
        }
        for(int i=-3;i<=0;i++){
            boolean win = true;
            //check row
            for(int j=0;j<4;j++){
                if(!withinBoard(col+i+j,row)){
                    win = false;
                    break;
                }
                if(j != -i && board[col+i+j][row] != iD){
                    win = false;
                    break;
                }
            }
            if(win){
                return true;
            }

            // check cols
            win = true;
            for(int j=0;j<4;j++){
                if(!withinBoard(col,row+i+j)){
                    win = false;
                    break;
                }
                if(j != -i && board[col][row+i+j] != iD){
                    win = false;
                    break;
                }
            }
            if(win){
                return true;
            }

            //check bottom left -> top-right diags
            win = true;
            for(int j=0;j<4;j++){
                if(!withinBoard(col+i+j,row-i-j)){
                    win = false;
                    break;
                }
                if(j != -i && board[col+i+j][row-i-j] != iD){
                    win = false;
                    break;
                }
            }
            if(win){
                return true;
            }

            //check top left -> bottom right diags
            win = true;
            for(int j=0;j<4;j++){
                if(!withinBoard(col+i+j,row+i+j)){
                    win = false;
                    break;
                }
                if(j != -i && board[col+i+j][row+i+j] != iD){
                    win = false;
                    break;
                }
            }
            if(win){
                return true;
            }

        }
        return false;
    }

    public String getName(){
        return this.name;
    }
}
